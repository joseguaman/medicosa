var express = require('express');
var router = express.Router();
//var rapidin = require('../controlador/utiles');
var test_file = require('../controlador/Test');
var test = new test_file();
var medicocontrol = require('../controlador/MedicoController');
var medico = new medicocontrol();
var cuentaC = require('../controlador/CuentaController');
var cuenta = new cuentaC();
var pacienteC = require('../controlador/PacienteController');
var paciente = new pacienteC();
//middleware sesiones
function validarSesion(req) {
    return (req.session !== undefined && req.session.peluche !== undefined);
}
var auth = function (req, res, next) {
    if(validarSesion(req)) {
        next();
    } else {
        req.flash('error', 'Inicie sesion primero');
        res.redirect('/');
    }
}
/* GET home page. */ 
router.get('/', function(req, res, next) {
    if(req.session !== undefined && req.session.peluche !== undefined) {
        res.render('index', { title: 'Principal', andrea:"fragmentos/principal", sesion: true, usuario: req.session.peluche.usuario});
    } else {
        res.render('index', { title: 'Medicos', msg: {error: req.flash('error'), info: req.flash('info')}});
    }
  
});

router.get('/registro', function(req, res, next) {
  res.render('index', { title: 'Registrate', sesion: true, andrea:"fragmentos/registro_medico" });
});
router.post('/registro', medico.guardar);
//inicio de sesion
router.post('/inicio_sesion', cuenta.iniciar_sesion);
router.get('/cerrar_sesion', auth, cuenta.cerrar_sesion);
//paciente
router.get('/administracion/pacientes', auth, paciente.visualizar);
router.post('/administracion/pacientes/guardar', auth, paciente.guardar);
router.get('/administracion/pacientes/modificar/:external', auth, paciente.visualizar_modificar);
router.post('/administracion/pacientes/update', auth, paciente.modificar);
//test
router.get('/medicos', auth, function (req, res) {
    res.send("Si puedes ver esto porq ya has entrado al sistema");        
});
router.get('/modificar', test.modificar);
//router.get('/viviana/:a/:b', rapidin.sumar);
/*router.get('/resta/:a/:b', function (req, res){
    var a = req.params.a * 1;
    var b = req.params.b * 1;
    var c = rapidin.resta(a, b);
    res.send(c+"");
});*/
//router.get('/medicos', test.listar);
//router.get('/medicos/guardar', test.guardar);
//router.post('/test/tabla', test.tabla);

module.exports = router;
