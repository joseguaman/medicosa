var express = require('express');
var router = express.Router();

var pacienteC = require('../controlador/PacienteController');
var paciente = new pacienteC();
//middleware sesiones
function validarSesion(req) {
    return (req.session !== undefined && req.session.peluche !== undefined);
}
var auth = function (req, res, next) {
    if(validarSesion(req)) {ttp://localhost:666/api/buscar_paciente
        next();
    } else {        
        res.redirect('/api');
    }
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({msg: "Debes iniciar sesion", codigo: 403});
});
router.get('/buscar_paciente',  paciente.buscar);
module.exports = router;
