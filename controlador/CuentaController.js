'use strict';
var cuentaC = require('../modelos/cuenta');
/**
 * Clase controladora que permite la gestion de inicio de sesion
 */
class Cuenta_Controller {
    /**
     * Permite iniciar sesion
     * @param {type} req objeto peticion
     * @param {type} res objeto respuesta
     * @returns {undefined} redirecciona segun su contenido
     */
    iniciar_sesion(req, res) {        
        cuentaC.getJoin({medico: true}).filter({correo: req.body.correo}).run()
                .then(function (andrea) {
            if(andrea.length > 0) {
                var cuenta = andrea[0];
                if(cuenta.clave === req.body.clave) {                    
                    req.session.peluche = {id:cuenta.id_medico, 
                        usuario: (cuenta.medico.apellidos + " " + cuenta.medico.nombres)};                 
                    res.redirect('/');
                } else {
                    req.flash('error', 'Sus credenciales no son las correctas!');
                    res.redirect('/');
                }                
            } else {
                req.flash('error', 'Sus credenciales no son las correctas!');
                res.redirect('/');
            }            
        });
    }
    cerrar_sesion(req, res) {
        req.flash('info', 'Ha salido del sistema!');
        req.session.destroy();        
        res.redirect('/');
    }
}
module.exports = Cuenta_Controller;
