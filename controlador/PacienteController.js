'use strict';
var pacienteC = require('../modelos/paciente');
var historiaC = require('../modelos/historia');
class PacienteController {
    visualizar(req, res) {
        pacienteC.getJoin({historia: true}).then(function (lista) {
            //pacienteC.count().execute().then(function (totales) {
            var nro_historia = 'HIS-' + (lista.length + 1);
            res.render('index',
                    {title: 'Pacientes',
                        andrea: "fragmentos/paciente/lista",
                        sesion: true,
                        nro: nro_historia,
                        listado: lista,
                        msg: {error: req.flash('error'), info: req.flash('info')}
                    });
            /*}).error(function (error) {
             req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
             res.redirect('/');
             });*/
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/');
        });

    }

    guardar(req, res) {
        var dataP = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            direccion: req.body.direccion
        };
        var paciente = new pacienteC(dataP);
        var dataH = {
            nro_historia: req.body.nro_his,
            enfermedades: req.body.enf,
            enfer_hede: req.body.enf_her,
            habitos: req.body.hab,
            contacto: req.body.contacto
        };
        var historia = new historiaC(dataH);
        paciente.historia = historia;
        paciente.saveAll({historia: true}).then(function (result) {
            req.flash('info', 'Se ha registrado correctamente');
            res.redirect('/administracion/pacientes');
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/administracion/pacientes');
        });
    }

    visualizar_modificar(req, res) {
        var external = req.params.external;
        pacienteC.getJoin({historia: true}).filter({external_id: external}).then(function (yanela) {
            if (yanela.length > 0) {
                var rojas = yanela[0];
                res.render('index',
                        {title: 'Pacientes',
                            andrea: "fragmentos/paciente/modificar",
                            sesion: true,
                            paci: rojas,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            } else {
                req.flash('error', 'No existe el dato a buscar');
                res.redirect('/administracion/pacientes');
            }
        }).error(function (error) {
            req.flash('error', 'se produjo un error');
            res.redirect('/administracion/pacientes');
        });

    }

    modificar(req, res) {
        pacienteC.getJoin({historia: true}).filter({external_id: req.body.token}).then(function (yanela) {
            if (yanela.length > 0) {
                var rojas = yanela[0];

                rojas.historia.nro_historia = req.body.nro_his;
                rojas.historia.enfermedades = req.body.enf;
                rojas.historia.enfer_hede = req.body.enf_her;
                rojas.historia.habitos = req.body.hab;
                rojas.historia.contacto = req.body.contacto;


                rojas.apellidos = req.body.apellidos;
                rojas.nombres = req.body.nombres;
                rojas.fecha_nac = req.body.fecha_nac;
                rojas.edad = req.body.edad;
                rojas.direccion = req.body.direccion;

                rojas.saveAll({cuenta: true}).then(function (result) {
                    req.flash('info', 'Se ha modificado correctamente');
                    res.redirect('/administracion/pacientes');
                }).error(function (error) {
                    console.log(error);
                    req.flash('error', 'No se pudo modificar');
                    res.redirect('/administracion/pacientes');
                });


            } else {
                req.flash('error', 'No existe el dato a buscar');
                res.redirect('/administracion/pacientes');
            }
        }).error(function (error) {
            req.flash('error', 'se produjo un error');
            res.redirect('/administracion/pacientes');
        });
    }

    buscar(req, res) {

        var criterio = req.query.criterio;
        var texto = req.query.texto;
        var data = {};
        if (criterio === 'todos') {
            pacienteC.getJoin({historia: true}).then(function (lista) {

                lista.forEach(function (item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro: item.historia.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function (error) { });
        } else if (criterio === "cedula") {
            pacienteC.getJoin({historia: true}).filter({cedula: texto}).then(function (lista) {

                lista.forEach(function (item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro: item.historia.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function (error) { });
        } else if (criterio === "historial") {
            pacienteC                    
                    .getJoin({
                        historia: true
                    })                    
                    .filter(function (data){                      
                        return data('historia')('nro_historia').eq(texto);
                    })
                    .then(function (lista) {                        
                        lista.forEach(function (item, index) {
                            data[index] = {
                                cedula: item.cedula,
                                paciente: item.apellidos + " " + item.nombres,
                                nro: item.historia.nro_historia,
                                external: item.external_id
                            };
                        });
                        res.json(data);
                    }).error(function (error) { });
        } else if (criterio === "apellidos") {
            pacienteC                    
                    .getJoin({
                        historia: true
                    })                    
                    .filter(function (data){                      
                        return  data('apellidos').match(texto);
                    })
                    .then(function (lista) {
                        
                        lista.forEach(function (item, index) {
                            data[index] = {
                                cedula: item.cedula,
                                paciente: item.apellidos + " " + item.nombres,
                                nro: item.historia.nro_historia,
                                external: item.external_id
                            };
                        });
                        res.json(data);
                    }).error(function (error) { });
        }

        //console.log(req.query.criterio);
        //res.json(req.query.criterio);
    }

}
module.exports = PacienteController;
