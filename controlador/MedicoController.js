'use strict';
var medico = require('../modelos/medico');
var cuenta = require('../modelos/cuenta'); 
class Medicocontroller {
    guardar(req, res) {
        var datos = {
            cedula: req.body.cedula,
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            nro_registro: req.body.nro,
            especialdidad: req.body.especialidad
        };

        var datosC = {
            correo: req.body.correo,
            clave: req.body.clave
        };
        //se crea el objeto del modelo y se carga los datos
        var Medico = new medico(datos);
        var Cuenta = new cuenta(datosC);
        Medico.cuenta = Cuenta;
        //se llama al metodo guardar
        Medico.saveAll({cuenta: true}).then(function (result) {
            res.redirect("/");
        }).error(function (error) {
            res.send(error);
        });
    }
}
module.exports = Medicocontroller;
