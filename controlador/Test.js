"use strict";
var utiles = require('./utiles');
var medico = require('../modelos/medico');
var cuenta = require('../modelos/cuenta');
class Test {
    listar(req, res) {
        medico.get("1").getJoin({cuenta: true})
                //medico
                //medico.getJoin({cuenta: true})
                .run().then(function (user) {
            console.log(user);
            res.send("Hola");
            /*
             * user = {
             *     id: "0e4a6f6f-cc0c-4aa5-951a-fcfc480dd05a",
             *     name: "Michel",
             *     account: {
             *         id: "3851d8b4-5358-43f2-ba23-f4d481358901",
             *         userId: "0e4a6f6f-cc0c-4aa5-951a-fcfc480dd05a",
             *         sold: 2420
             *     }
             * }
             */
        }).error(utiles.handleError(res));
    }
    guardar(req, res) {
        var data = {cedula: "1104334634", apellidos: "Guaman", nombres: "sebastian", edad: 12};
        var med = new medico(data);
        var datacuenta = {correo: "sebas@unl.edu.ec", clave: "1234"};
        var cuen = new cuenta(datacuenta);
        //med.cuenta = [cuen];      
        med.cuenta = cuen;
        //cuen.medico = med;
        med.saveAll({cuenta: true}).then(function (result) {
            res.send(result);
        }).error(function (error) {
            // Handle error
        });
    }

    modificar(req, res) {
        medico.get("ca53419e-7a18-4026-9b9a-55435ddd798b").getJoin({cuenta: true}).run().then(function (user) {
            // ...
            user.apellidos = "Rojitas castro";
            user.cuenta.clave = "123456";
            user.saveAll({cuenta: true}).then(function (post) {
                res.send(post);
            }).error(function (error) {
                res.send(error);
            });
            //{apellidos: "Rojas castro", cuenta: {clave:"123456"}})

        }).error(function (error) {
            // ...
            res.send(error);
        });
    }

    tabla(req, res) {
        var a = req.body.nra;
        var b = req.body.nrb;
        var resp = {};
        for (var i = 1; i <= b; i++) {
            resp[i - 1] = {"resp": (i * a), "detalle": i + "*" + a};
        }
        //res.send(resp);
        res.render('index', {title: "tabla", andrea: 'fragmentos/tabla', sesion: true, respuesta: resp});
    }

}
module.exports = Test;